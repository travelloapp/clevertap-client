package com.travelloapp.clevertap.model;

public enum ResponseStatus {
    success, partial, fail
}
