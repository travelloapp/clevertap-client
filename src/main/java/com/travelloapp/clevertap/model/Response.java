package com.travelloapp.clevertap.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    private ResponseStatus status;
    private int processed;
    private List<ResponseErrorDetail> unprocessed;

    public boolean isSuccess() {
        return ResponseStatus.success.equals(this.status);
    }
}

