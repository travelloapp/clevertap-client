package com.travelloapp.clevertap;

import com.travelloapp.clevertap.model.ProfileUpdate;
import com.travelloapp.clevertap.model.Request;
import com.travelloapp.clevertap.model.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static java.lang.String.format;

@Slf4j
public class CleverTapClient {
    private String cleverTapAccountId;
    private String cleverTapAccountPasscode;
    private String cleverTapUrl;
    private RestTemplate restTemplate;

    public CleverTapClient(
            String cleverTapAccountId,
            String cleverTapAccountPasscode,
            String cleverTapUrl,
            boolean enablePayloadLogging) {
        this.cleverTapAccountId = cleverTapAccountId;
        this.cleverTapAccountPasscode = cleverTapAccountPasscode;
        this.cleverTapUrl = cleverTapUrl;
        this.restTemplate = configureRestTemplate(enablePayloadLogging);
    }

    private RestTemplate configureRestTemplate(boolean enablePayloadLogging) {
        BufferingClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
        RestTemplate restTemplate = new RestTemplate(factory);
        log.info("configured with enablePayloadLogging: {}", enablePayloadLogging);
        restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor(enablePayloadLogging)));
        return restTemplate;
    }

    public void executePost(ProfileUpdate profileUpdate) {
        HttpHeaders headers = createHeaders();
        Request request = createPayload(profileUpdate);
        log.debug("cleverTapRequest: {}", request);
        log.info("Post profile {}", profileUpdate.getIdentity());

        try {
            HttpEntity<Request> requestPayload = new HttpEntity<>(request, headers);
            ResponseEntity<Response> response = restTemplate.postForEntity(cleverTapUrl, requestPayload, Response.class);
            log.info("Got response for profile {}: {}", profileUpdate.getIdentity(), response);
            Response responsePayload = response.getBody();
            if (!responsePayload.isSuccess()) {
                throw new RuntimeException(buildErrorMessage(request, responsePayload.toString()));
            }
        } catch (HttpClientErrorException e) {
            throw new RuntimeException(buildErrorMessage(request, e.getResponseBodyAsString()), e);
        }
    }

    private String buildErrorMessage(Request request, String responseBodyAsString) {
        return format("Failed to post to CleverTaps Service url: [%s], payload: [%s] -> response body: [%s]", cleverTapUrl, request, responseBodyAsString);
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("X-CleverTap-Account-Id", cleverTapAccountId);
        headers.set("X-CleverTap-Passcode", cleverTapAccountPasscode);
        return headers;
    }

    private Request createPayload(ProfileUpdate profileUpdate) {
        Request request = new Request();
        request.setProfileUpdates(new ProfileUpdate[]{profileUpdate});
        return request;
    }

}
