package com.travelloapp.clevertap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

@Slf4j
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

    private boolean enablePayloadLog;

    public RequestResponseLoggingInterceptor(boolean enablePayloadLog) {
        this.enablePayloadLog = enablePayloadLog;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        logRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        logResponse(response);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) {
        if (enablePayloadLog) {
            log.debug("Request to uri: {}, method: {},  body: {}", request.getURI(), request.getMethod(), new String(body));
        }
    }

    private void logResponse(ClientHttpResponse response) throws IOException {
        if (enablePayloadLog) {
            log.debug("Response status: {}, body: {}", response.getStatusCode(), StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        }
    }
}
