package com.travelloapp.clevertap.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseErrorDetail {
    private ResponseStatus status;
    private String code;
    private String error;
    private String record;
}
