package com.travelloapp.clevertap.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Request {
    @JsonProperty("d")
    private ProfileUpdate[] profileUpdates;
}