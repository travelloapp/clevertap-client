package com.travelloapp.clevertap.model;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.assertj.core.api.Assertions.assertThat;

public class DateToStringConverterTest {

    private DateToStringConverter converter = new DateToStringConverter();

    @Before
    public void setUp() {
        TimeZone.setDefault( TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void convertDatesPriorToEpochStart() throws Exception {
        // date command on macOs: date -ju -f "%FT%TZ" 1969-12-31T00:00:00Z +%s
        assertThat(converter.convert(toDate("1968-06-13"))).isEqualTo("$D_-48988800");
        assertThat(converter.convert(toDate("1970-06-13"))).isEqualTo("$D_14083200");
        assertThat(converter.convert(toDate("1978-01-29"))).isEqualTo("$D_254880000");
        assertThat(converter.convert(toDate("1970-01-01"))).isEqualTo("$D_0");
        assertThat(converter.convert(toDate("1969-12-31"))).isEqualTo("$D_-86400");
        assertThat(converter.convert(toDate("1969-12-30"))).isEqualTo("$D_-172800");
        assertThat(converter.convert(toDate("2000-05-27"))).isEqualTo("$D_959385600");
    }

    private Date toDate(String dateStr) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
    }
}