package com.travelloapp.clevertap.model;

import lombok.Data;

@Data
public class DeleteDirective implements GenderAware {
    private int delete = 1;
}
