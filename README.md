# README #

A client to use for integration with Clever Tap.

For properties required to be configured see CleverTapClient.

For example payloads see CleverTapClientTest output.