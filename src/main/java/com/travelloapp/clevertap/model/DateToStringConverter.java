package com.travelloapp.clevertap.model;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Date;

public class DateToStringConverter extends StdConverter<Date, String> {

    @Override
    public String convert(Date date) {
        if (date == null) {
            return null;
        }
        long timestampInMillis = date.getTime();
        long timestampInSec = timestampInMillis/1000;
        return String.format("$D_%d", timestampInSec);
    }
}
