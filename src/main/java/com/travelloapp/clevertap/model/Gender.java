package com.travelloapp.clevertap.model;

public enum Gender implements GenderAware {
    F, M
}
