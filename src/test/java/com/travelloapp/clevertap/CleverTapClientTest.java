package com.travelloapp.clevertap;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.travelloapp.clevertap.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class CleverTapClientTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void marshalToJsonPayloadWithUnknownGender() throws Exception {

        Request request = createRequest(new DeleteDirective());

        String actualJson = objectMapper.writeValueAsString(request);
        log.info("actualJson: {}", actualJson);

        // pretty crude, could be using JSONAssert or similar tool, but for now - this will do
        String expectedJson = "{\"d\":[{\"type\":\"profile\",\"identity\":\"3333-111-12abc-abb6-1111\",\"profileData\":{\"email\":\"testProfile@test.com\",\"userName\":\"testProfile1\",\"gender\":{\"delete\":1},\"city\":\"Butcher's Creek\",\"dob\":\"$D_-93";
        assertThat(actualJson).startsWith(expectedJson);
        assertThat(actualJson).contains("\"rewards_balance\":294732748");
    }

    @Test
    public void marshalToJsonPayloadWithKnownGender() throws Exception {

        Request request = createRequest(Gender.F);

        String actualJson = objectMapper.writeValueAsString(request);
        log.info("actualJson: {}", actualJson);

        String expectedJson = "{\"d\":[{\"type\":\"profile\",\"identity\":\"3333-111-12abc-abb6-1111\",\"profileData\":{\"email\":\"testProfile@test.com\",\"userName\":\"testProfile1\",\"gender\":\"F\",\"city\":\"Butcher's Creek\",\"dob\":\"$D_-93";
        assertThat(actualJson).startsWith(expectedJson);
    }

    private Request createRequest(GenderAware genderAware) {
        ProfileUpdate profileUpdate = createProfileUpdate(genderAware);

        Request request = new Request();
        request.setProfileUpdates(new ProfileUpdate[]{profileUpdate});
        return request;
    }

    private ProfileUpdate createProfileUpdate(GenderAware genderAware) {
        ProfileUpdate profileUpdate = new ProfileUpdate();
        profileUpdate.setIdentity("3333-111-12abc-abb6-1111");

        ProfileData profileData = new ProfileData();
        profileData.setEmail("testProfile@test.com");
        profileData.setUserName("testProfile1");
        profileData.setGender(genderAware);
        profileData.setRewardsBalance(294732748l);
        profileData.setDob(toDate(LocalDate.of(1940, 6, 20)));
        profileData.setCity("Butcher's Creek");

        profileUpdate.setProfileData(profileData);
        return profileUpdate;
    }

    public Date toDate(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

}