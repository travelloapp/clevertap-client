package com.travelloapp.clevertap.model;

/**
 * The only reason this exists is for ability to provide Delete functionality which is modelled as an object in Clever Tap REST API
 */
public interface GenderAware { }