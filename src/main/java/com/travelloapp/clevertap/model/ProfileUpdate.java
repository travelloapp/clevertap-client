package com.travelloapp.clevertap.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@JsonInclude(NON_NULL)
public class ProfileUpdate {
    private String type = "profile";
    private String identity;
    private ProfileData profileData;
}