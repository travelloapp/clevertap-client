package com.travelloapp.clevertap.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@JsonInclude(NON_NULL)
public class ProfileData {
    private String email;

    private String userName;

    private GenderAware gender;

    private String city;

    @JsonSerialize(converter = DateToStringConverter.class)
    private Date dob;

    private String profileImage;

    @JsonProperty("MSG-email")
    private Boolean emailSubscription;

    @JsonProperty("rewards_balance")
    private Long rewardsBalance;
}